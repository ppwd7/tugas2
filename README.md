[![pipeline status](https://gitlab.com/ppwd7/tugas2/badges/master/pipeline.svg)](https://gitlab.com/ppwd7/tugas2/commits/master)
[![coverage report](https://gitlab.com/ppwd7/tugas2/badges/master/coverage.svg)](https://gitlab.com/ppwd7/tugas2/commits/master)

# Tugas 2 PPW
Kelas D kelompok 7, anggota:
- Azhar Kurnia
- Raka Pramada
- Wildan Fahmi Gunawan
- Yudhistira Erlandinata

[https://winnerswin.herokuapp.com](https://winnerswin.herokuapp.com)