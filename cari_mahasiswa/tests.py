from django.test import TestCase
from django.test import Client
from django.urls import reverse
from profile_page import get_profile_by_npm
from profile_page.models import Profile

class CariMahasiswaUnitTest(TestCase):
    def test_cari_mahasiswa_url_is_exist(self):
        self.__login()
        profile = self.__get_dummy_profile()
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 301)
        response = self.client.get('/cari_mahasiswa/')
        self.assertEqual(response.status_code,200)

    def __login(self):
        session = self.client.session
        npm = self.__get_dummy_profile().npm
        session['kode_identitas'] = npm
        session.save()
        return session

    def __get_dummy_profile(self):
        profile = Profile(npm='1606889591', name='Wildan Fahmi') if not get_profile_by_npm('1606889591') else get_profile_by_npm('1606889591')
        profile.save()
        return profile