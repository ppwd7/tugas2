# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 20:29
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cari_mahasiswa', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='mahasiswa',
        ),
    ]
