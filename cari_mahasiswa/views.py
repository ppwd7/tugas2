from profile_page import render_with_profile as render
from profile_page import get_list_of_profile_by_name, get_profile_render_data
from login_status import require_login

response ={}

@require_login
def index(request):
    mahasiswa_list = get_list_of_profile_by_name('')
    data = []
    for mahasiswa in mahasiswa_list:
        data.append(get_profile_render_data(mahasiswa.npm))

    # mahasiswa_list = Mahasiswa.objects.all()
    html = 'cari_mahasiswa/cari_mahasiswa.html'
    response={"mahasiswa_list" : data}
    return render(request, html, response)

# Create your views here.