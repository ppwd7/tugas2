from django.test import TestCase
from riwayat_page import api_riwayat
from django.http import HttpRequest
from django.urls import reverse
from profile_page import should_profile_shows_score, get_profile_by_npm
from profile_page.models import Profile

class RiwayatApiUnitTest(TestCase):
    
    def setUp(self):
        super().setUp()
        # mock dependency for faster testing: requests.get
        #
        self.original_get_request = api_riwayat.requests.get
        class FakeResponse:
            def json(self):
                return {}
        def fake_request(url):
            return FakeResponse()
        api_riwayat.requests.get = fake_request

    def tearDown(self):
        api_riwayat.requests.get = self.original_get_request

    def test_get_riwayat(self):
        self.assertTrue(api_riwayat.get_riwayat())

    def __login(self):
        session = self.client.session
        npm = self.__get_dummy_profile().npm
        session['kode_identitas'] = npm
        session.save()
        return session

    def __get_dummy_profile(self):
        profile = Profile(npm='1606894534', name='Yudhistira Erlandinata') if not get_profile_by_npm('1606894534') else get_profile_by_npm('1606894534')
        profile.save()
        return profile


    def test_render_page(self):
        self.__login()
        profile = self.__get_dummy_profile()
        profile.npm = '123131'
        profile.save()
        request = HttpRequest()
        response = self.client.get(reverse('riwayat:index'))
        self.assertEqual(response.status_code, 200)
       
