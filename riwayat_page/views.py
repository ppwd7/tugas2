from profile_page import render_with_profile as render

# Create your views here.
from .api_riwayat import get_riwayat
from login_status.utils import require_login, get_logged_in_user_npm
from profile_page import should_profile_shows_score, get_profile_by_npm


response = {'riwayat_page': True}
list_riwayat = get_riwayat().json()
@require_login
def index(request):
	npm = get_logged_in_user_npm(request)
	response['list_riwayat'] = get_riwayat().json()
	response['show_score'] = should_profile_shows_score(npm)
	return render(request,'riwayat.html',response)
