from django.db import models

# Create your models here.
class Profile(models.Model):
    npm = models.CharField(max_length=32, primary_key=True)
    name = models.CharField(max_length=1286, null=True)
    email = models.CharField(max_length=646, null=True)
    linkedin = models.CharField(max_length=256, null=True)
    headline = models.TextField(null=True)
    picture_url = models.TextField(null=True)
    showing_score = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} ({}): {}'.format(self.name if self.name else 'unnamed', self.npm, self.email if self.email else 'no email')

    def is_connected_to_linkedin(self):
        return self.linkedin is not None

    def get_angkatan(self):
        return '20' + str(self.npm)[:2]

class Skill(models.Model):
    INTERMEDIATE = 'I'
    ADVANCED = 'A'
    EXPERT = 'E'
    LEGEND = 'L'
    SKILL_LEVELS = (
        (INTERMEDIATE, 'Intermediate'),
        (ADVANCED, 'Advanced'),
        (EXPERT, 'Expert'),
        (LEGEND, 'Legend'),
    )
    profile = models.ForeignKey(Profile)
    skill_title = models.CharField(max_length=32)
    skill_level = models.CharField(
        max_length=1,
        choices=SKILL_LEVELS,
        default=INTERMEDIATE,
    )

    def __str__(self):
        return '{} ({}): {}'.format(self.skill_title, self.get_skill_level_display(), self.profile.name)

class Status(models.Model):
    account = models.ForeignKey(Profile)
    message = models.TextField(max_length=300)
    created_date = models.DateTimeField(auto_now_add=True)

