# Fitur Profile
by Yudhistira Erlandinata

## Base HTML
Gunakan 'profile/base.html'

## Mencari profile dengan npm
```python
from profile_page import get_profile_by_npm
profile = get_profile_by_npm(npm)
if profile: # PROFILE ADA
else: # Profile tidak ada
```

## Mendapatkan list profile dengan nama
```python
from profile_page import get_list_of_profile_by_name
profiles = get_list_of_profile_by_name(name)
if profiles: # PROFILE ADA
else: # Profile tidak ada
```
